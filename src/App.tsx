import React from 'react'
import Task from './components/Task'
import { useState } from 'react'
import { ITask } from './interface'
import InputTask from './components/InputTask'
import {
    DragDropContext,
    Draggable,
    Droppable,
    DropResult,
} from 'react-beautiful-dnd'

const reorder = (
    list: ITask[],
    startIndex: number,
    endIndex: number
): ITask[] => {
    const result = Array.from(list)
    const [removed] = result.splice(startIndex, 1)
    result.splice(endIndex, 0, removed)
    return result
}

function App() {
    const [list, setList] = useState<ITask[]>([])

    const handleAddNewTask = (newTask: ITask): void => {
        setList([...list, newTask])
    }

    const handleRemoveTask = (id: number) => () => {
        setList(list.filter((p) => p.id !== id))
    }

    const handleChangeTask = (task: ITask) => () => {
        setList(
            list.map((t) => {
                return t.id === task.id ? task : t
            })
        )
    }

    const onDragEndHandler = (result: DropResult): void => {
        if (!result.destination) {
            return
        }
        const tasks: ITask[] = reorder(
            list,
            result.source.index,
            result.destination.index
        )
        setList(tasks)
    }
    return (
        <div className="flex m-top-50 items-center justify-center flex-col">
            <h1 className="text-5xl my-5 text-white font-bold">To do list</h1>
            <InputTask setList={handleAddNewTask} />
            <DragDropContext onDragEnd={onDragEndHandler}>
                <Droppable droppableId="droppable">
                    {(provided, snapshot): JSX.Element => (
                        <div
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                        >
                            {list.map((task, index) => (
                                <Draggable
                                    key={task.id}
                                    draggableId={task.id.toString()}
                                    index={index}
                                >
                                    {(provided, snapshot): JSX.Element => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                        >
                                            <Task
                                                task={task}
                                                key={task.id}
                                                remove={handleRemoveTask}
                                                onChange={handleChangeTask}
                                            />
                                        </div>
                                    )}
                                </Draggable>
                            ))}
                        </div>
                    )}
                </Droppable>
            </DragDropContext>
        </div>
    )
}

export default App
