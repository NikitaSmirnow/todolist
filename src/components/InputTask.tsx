import { useState } from 'react'
import { ChangeEvent } from 'react'
import { ITask } from '../interface'

type Props = { setList: (item: ITask) => void }

const InputTask = (props: Props) => {
    const { setList } = props
    const [taskColor, setTaskColor] = useState('bg-white')
    const [task, setTask] = useState('')

    const addTask = (): void => {
        if (task.length !== 0) {
            const newTask = {
                value: task,
                id: Date.now(),
                color: taskColor,
                complete: false,
            }
            setList(newTask)
            setTaskColor('bg-white')
            setTask('')
        }
    }

    const changeHandler = (event: ChangeEvent<HTMLInputElement>): void => {
        setTask(event.target.value)
    }
    const chooseColor = ({ currentTarget }: any) =>
        setTaskColor(currentTarget.value)

    return (
        <div className={`flex flex-row ${taskColor} py-4 px-2`}>
            <div className="border-2 border-gray-400 flex flex-row px-2 bg-white">
                <input
                    placeholder="task"
                    className="py-2 outline-0"
                    onChange={changeHandler}
                    value={task}
                />
                <div className="flex flex-row w-20 items-center place-content-between bg-white">
                    <button
                        className="bg-red-300 w-5 h-5 rounded-lg"
                        name="red"
                        value="bg-red-400"
                        onClick={chooseColor}
                    ></button>
                    <button
                        className="bg-yellow-300 w-5 h-5 rounded-lg"
                        name="yellow"
                        value="bg-yellow-400"
                        onClick={chooseColor}
                    ></button>
                    <button
                        className="bg-green-300 w-5 h-5 rounded-lg"
                        name="green"
                        value="bg-green-400"
                        onClick={chooseColor}
                    ></button>
                </div>
            </div>
            <button
                type="button"
                className="border px-4 py-2 bg-blue-400 text-white"
                onClick={addTask}
            >
                Add
            </button>
        </div>
    )
}

export default InputTask
