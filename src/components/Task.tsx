import { ITask } from '../interface'

export interface Props {
    task: ITask
    remove: (id: number) => () => void
    onChange: (task: ITask) => () => void
}

const Task = ({ task, remove, onChange }: Props) => {
    return (
        <div
            className={`flex flex-row bg-white w-[355px] my-2 py-2 place-content-between px-2 items-center cursor-grab ${task.color}`}
        >
            <input
                type="checkbox"
                className="w-7 h-7"
                onChange={onChange({ ...task, complete: !task.complete })}
            />
            <p className={`break-words ${task.complete ? 'line-through' : ''}`}>
                {task.value}
            </p>
            <button onClick={remove(task.id)} className="w-10 h-10">
                <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Trash_bag_illustration.svg/1200px-Trash_bag_illustration.svg.png"
                    alt="trash"
                />
            </button>
        </div>
    )
}

export default Task
