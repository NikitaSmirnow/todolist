export interface ITask{
  id:number,
  value:string,
  color:string, 
  complete:boolean,
}